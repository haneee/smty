resource "aws_cloudformation_stack" "macie-cloudformation" {
    capabilities = ["CAPABILITY_IAM"]
    disable_rollback = false
    name = "macie-cloudformation"
    template_body =file("${path.module}/source/macie.yaml")
    
}