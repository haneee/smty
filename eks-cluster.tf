resource "aws_iam_role" "eks-cluster-role" {
  name = "eks-cluster-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

// Policies attached to EKS Cluster
// 1. EKSClusterPolicy, 2. EKSServicePolicy, 3. EKSVPCResourceController
resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-cluster-role.name
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks-cluster-role.name
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks-cluster-role.name
}


## EKS 클러스터를 정의합니다
resource "aws_eks_cluster" "eks-cluster" {
    name                      = "eks-cluster-2"
    role_arn                  = aws_iam_role.eks-cluster-role.arn
    version                   = 1.27
    enabled_cluster_log_types =  [ "api", "audit", "authenticator", "controllerManager", "scheduler"] 
    vpc_config {
        subnet_ids              = [aws_subnet.my-pub-2a.id, aws_subnet.my-pub-2c.id]
        security_group_ids      = [aws_security_group.my-sg-web.id]
        endpoint_private_access = "true"
        endpoint_public_access  = "true"
    }

    tags = {
        Name        = "eks-cluster-2"
    }

    depends_on = [
        aws_iam_role_policy_attachment.cluster_AmazonEKSClusterPolicy,
        aws_iam_role_policy_attachment.cluster_AmazonEKSServicePolicy,
        aws_iam_role_policy_attachment.cluster_AmazonEKSVPCResourceController,
    ]
}
