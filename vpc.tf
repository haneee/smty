locals {
    cidr            = "10.36.0.0/16"
    public_subnets  = ["10.36.0.0/20", "10.36.32.0/20"]
    private_subnets = ["10.36.64.0/20", "10.36.96.0/20", "10.36.80.0/20", "10.36.112.0/20"]
    azs             = ["ap-northeast-2a", "ap-northeast-2c", "ap-northeast-2b", "ap-northeast-2d"]
    
}

resource "aws_vpc" "my-vpc" {
  cidr_block  = "10.36.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"
  tags = {
    Name = "my-vpc"
  }
}

resource "aws_subnet" "my-pub-2a" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.public_subnets[0]
  availability_zone       = local.azs[0]
  map_public_ip_on_launch = true # 공개 IP를 부여
  tags = {
    Name = "my-pub-2a"
  }
}

resource "aws_subnet" "my-pub-2c" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.public_subnets[1]
  availability_zone       = local.azs[1]
  map_public_ip_on_launch = true # 공개 IP를 부여
  tags = {
    Name = "my-pub-2c"
  }
}

resource "aws_subnet" "my-pvt-2a" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.private_subnets[0]
  availability_zone       = local.azs[0]
  # map_public_ip_on_launch = true # 공개 IP를 부여
  tags = {
    Name = "my-pvt-2a"
  }
}

resource "aws_subnet" "my-pvt-2c" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.private_subnets[1]
  availability_zone       = local.azs[1]
  tags = {
    Name = "my-pvt-2c"
  }
}

resource "aws_subnet" "my-pvt-2b" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.private_subnets[2]
  availability_zone       = local.azs[2]
  tags = {
    Name = "my-pvt-2b"
  }
}

resource "aws_subnet" "my-pvt-2d" {
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.private_subnets[3]
  availability_zone       = local.azs[3]
  tags = {
    Name = "my-pvt-2d"
  }
}


resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.my-vpc.id
  tags = {
    Name = "my-igw-3"
  }
}

resource "aws_route" "internet-access" {
  route_table_id = aws_route_table.pub-rtb.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.my-igw.id
}


// elastic ip
resource "aws_eip" "EIP" {
  vpc   = true
}

resource "aws_nat_gateway" "EKS_NAT" {
  allocation_id = aws_eip.EIP.id
  subnet_id =  resource.aws_subnet.my-pub-2c.id
  tags = {
    Name = "EKS_NAT"
  }
}

# public round table
resource "aws_route_table" "pub-rtb" {
  vpc_id = aws_vpc.my-vpc.id
  # route {
  #   cidr_block = "10.36.0.0/16"
  #   gateway_id = aws_internet_gateway.my-igw.id
  # }
  tags = {
    Name = "pub-rtb"
  }
}
resource "aws_route" "add_igw" {
  route_table_id = aws_route_table.pub-rtb.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.my-igw.id
}

# private round table 
resource "aws_route_table" "pvt-rtb" {
  vpc_id=aws_vpc.my-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.EKS_NAT.id
  }
  tags = {
    Name = "pvt-rtb"
  }
}

# Create Public Route Table Routing
resource "aws_route_table_association" "TEST-PUBROUTING1" {
  route_table_id = aws_route_table.pub-rtb.id
  subnet_id      = aws_subnet.my-pub-2a.id
}
resource "aws_route_table_association" "TEST-PUBROUTING2" {
  route_table_id = aws_route_table.pub-rtb.id
  subnet_id      = aws_subnet.my-pub-2c.id
}
# Create Private Route Table Routing
resource "aws_route_table_association" "TEST-PVTROUTING1" {
  route_table_id = aws_route_table.pvt-rtb.id
  subnet_id      = aws_subnet.my-pvt-2a.id
}
resource "aws_route_table_association" "TEST-PVTROUTING2" {
  route_table_id = aws_route_table.pvt-rtb.id
  subnet_id      = aws_subnet.my-pvt-2c.id
}


