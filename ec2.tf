
# bastion host
resource "aws_instance" "bastion" {
  ami = "ami-0ea4d4b8dc1e46212"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.my-pub-2a.id
  key_name = "id_rsa"
  
  vpc_security_group_ids = [
    aws_security_group.my-sg-web.id
  ]

  tags = {
    "Name" = "NAT-bastion"
  }
}


# EC2 Agent
resource "aws_instance" "Elastic-agent-EC2" {
  ami = "ami-0ea4d4b8dc1e46212"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.my-pvt-2a.id
  key_name = "id_rsa"
  
  vpc_security_group_ids = [
    aws_security_group.my-sg-web.id
  ]

  tags = {
    "Name" = "Elastic-agent-EC2"
  }
}


# Lambda Agent
resource "aws_instance" "Elastic-agent-Lambda" {
  ami = "ami-0ea4d4b8dc1e46212"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.my-pvt-2a.id
  key_name = "id_rsa"
  
  vpc_security_group_ids = [
    aws_security_group.my-sg-web.id
  ]

  tags = {
    "Name" = "Elastic-agent-Lambda"
  }
}


# S3 Agent
resource "aws_instance" "Elastic-agent-S3" {
  ami = "ami-0ea4d4b8dc1e46212"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.my-pvt-2a.id
  key_name = "id_rsa"
  
  vpc_security_group_ids = [
    aws_security_group.my-sg-web.id
  ]

  tags = {
    "Name" = "Elastic-agent-S3"
  }
}


# Dev server
resource "aws_instance" "DEV-Server" {
  ami = "ami-04341a215040f91bb"
  instance_type = "t2.large"
  subnet_id = aws_subnet.my-pvt-2c.id
  key_name = "id_rsa"
  
  vpc_security_group_ids = [
    aws_security_group.my-sg-web.id
  ]

  tags = {
    "Name" = "DEV-Server"
  }
}