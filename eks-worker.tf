// EKS Worker Node
resource "aws_eks_node_group" "worker" {
  cluster_name    = aws_eks_cluster.eks-cluster.name
  node_group_name = "eks-worker-node"
  node_role_arn   = aws_iam_role.worker.arn
  subnet_ids      = [aws_subnet.my-pvt-2a.id, aws_subnet.my-pvt-2c.id] // Network Configuration
  
  // Worker Settings
  instance_types = ["t2.micro"]
  disk_size      = 8
  scaling_config {
    desired_size = 4
    min_size     = 4
    max_size     = 16
  }

  remote_access {
    source_security_group_ids = [aws_security_group.my-sg-web.id]
    ec2_ssh_key               = "id_rsa"
  }

  tags = {
    Name        = "eks-worker-node"
    Environment = "eks-worker-node"
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.eks-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.eks-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.eks-AmazonEC2ContainerRegistryReadOnly,
  ]
}



resource "aws_iam_role_policy_attachment" "eks-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.worker.name
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.worker.name
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.worker.name
}

// Worker Node Assume Role
data "aws_iam_policy_document" "workers_role_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

// IAM Role EC2관련 IAM Role을 생성해주고
resource "aws_iam_role" "worker" {
  name               = "eks-worker-role"
  assume_role_policy = data.aws_iam_policy_document.workers_role_assume_role_policy.json
  tags = {
    Environment = "eks-worker"
  }
}